<?php
	session_start();
	require_once('connection.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Grafika w ZSE</title>
		<link rel="stylesheet" href="./style.css"/>
		<link rel="stylesheet" href="./normalize.css"/>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:600,700" rel="stylesheet"/>
	</head>
	<body>
		<div id="main">
			<div id="top">
				Grafika komputerowa w ZSE
			</div>
			<div id="menu">
				<a href="http://zse1.zse.gda.pl/"><div id="mainsite">Powrót do strony głównej</div></a>
				<?php
					try{
						$conn = new PDO("mysql:host=$host;dbname=$db_name", $db_user, $db_pass);
						$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						$paramYears = $conn->prepare("SELECT yearID, name FROM years");
						$paramYears->execute();
						$rows = $paramYears->fetchAll();
						for($i = count($rows)-1; $i >= 0 ; $i--){
							echo '<a href="index.php?year='.$rows[$i][0].'"><div class="prace">Prace uczniów '.$rows[$i][1].'</div></a>';
						}
					}catch(PDOExeption $e){
						echo "Connection failed: ".$e->getMessage();
					}
					$conn = NULL;
				?>
				<a href="http://zse.gda.pl/prace-uczniow/"><div class="prace">Prace uczniów 2014/2015</div></a>
				<a href="http://zse.gda.pl/konkursy/'"><div id="konkursy">Konkursy</div></a>
			</div>
			<div id="cont">
				<div id="text">
					<?php
						if(!isset($_GET['year'])){
							echo '<div>Prace z grafiki komputerowej, realizacje fotogarficzne i filmowe są w naszej szkole realizowane m.in. podczas:<br/>
							<ul>
								<li>zajeć koła grafiki komputerowej,</li>
								<li>koła fotograficzno - filmowego,</li>
								<li>oraz podczas zajęć przedmiotowych kwalifikacji E.14.</li>
							</ul>
							Uczniowie realizują także materiały fotograficzne i filmowe z imprez szkolnych.<br/><br/>
							Prowadzący nauczyciel: <span style="font-weight: 700;">Marzena Parowińska</span></div>';
						}else if(isset($_GET['year']) && isset($_GET['type'])){
								try{
									$conn = new PDO("mysql:host=$host;dbname=$db_name", $db_user, $db_pass);
									$paramUrl = $conn->prepare("SELECT DISTINCT url FROM projects WHERE typeID=? AND yearID=?");
									$paramUrl->execute([$_GET['type'], $_GET['year']]);
									$url = $paramUrl->fetchAll(PDO::FETCH_COLUMN, 0);
									$paramAuthorID = $conn->prepare("SELECT authorID FROM projects WHERE url=?");
									$projects = array();
									for($i = 0; $i < count($url); $i++){
										$paramAuthorID->execute([$url[$i]]);
										$projects[$i] = $paramAuthorID->fetchAll(PDO::FETCH_COLUMN, 0);
									}
									$paramName = $conn->prepare("SELECT CONCAT(name, ' ', surname, ' ', class) AS fullName FROM authors WHERE authorID=?");
									$paramExtra = $conn->prepare("SELECT DISTINCT extra FROM projects WHERE url=?");
									for($i = 0; $i < count($projects); $i++){
										$name = array();
										if($_GET['type'] != 1){
											echo '<div class="tiles"><a href="'.$url[$i].'"><img src="'.dirname($url[$i], 1).'/mini.png"></a><br/>';
										}else{
											echo '<div class="tiles"><video width="320" height="240" controls muted><source src="'.$url[$i].'"></video><br/>';
										}
										$paramExtra->execute([$url[$i]]);
										$extra = $paramExtra->fetchAll(PDO::FETCH_COLUMN, 0);
										echo '<div class="names">';
										if($extra[0] != ""){
											echo '<span style="font-weight: 800;">'.$extra[0].'</span>';
										}
										echo '<div>';
										for($y = 0; $y < count($projects[$i]); $y++){
											$paramName->execute([$projects[$i][$y]]);
											$name = array_merge($name, $paramName->fetchAll(PDO::FETCH_COLUMN, 0));
											echo $name[$y].'<br/>';
										}
										echo '</div>';
										echo '</div>';
										echo '</div>';
									}
								}catch(PDOExeption $e){
									echo "Connection failed ".$e->getMessage();
								}
								$conn = NULL;
						}else if(isset($_GET['year'])){
							try{
								$conn = new PDO("mysql:host=$host;dbname=$db_name", $db_user, $db_pass);
								$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
								$paramTypes = $conn->prepare("SELECT DISTINCT typeID FROM projects WHERE yearID=?");
								$paramTypes->execute([$_GET['year']]);
								$types = $paramTypes->fetchAll(PDO::FETCH_COLUMN, 0);
								$typesName = $conn->prepare("SELECT name FROM typesOfProjects WHERE typeID=?");
								$names = array();
								for($i = 0; $i < count($types); $i++){
									$typesName->execute([$types[$i]]);
									$names = array_merge($names, $typesName->fetchAll(PDO::FETCH_COLUMN, 0));
								}
								echo '<div id="types">';
								for($i = count($types)-1; $i >= 0; $i--){
									echo '<a href="index.php?year='.$_GET['year'].'&type='.$types[$i].'"><div class="types" id="'.strtolower(str_replace(' ','_',$names[$i])).'">'.$names[$i].'</div></a>';
								}
								echo '</div>';
							}catch(PDOExeption $e){
								echo "Connection failed: ".$e->getMessage();
							}
							$conn = NULL;
						}
					?>
				</div>
			</div>
		</div>
	</body>
</html>
