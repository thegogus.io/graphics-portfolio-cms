<?php
  session_start();
  if(isset($_SESSION['logged']) && $_SESSION['logged'] == 1){
    header("location: cms.php");
    exit();
  }
?>
<DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <link rel="stylesheet" type="text/css" href="normalize.css"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:600" rel="stylesheet"/>
  </head>
  <body>
    <div id="form">
      <form action="logon.php" method="post">
      Login:<br/>
      <input type="text" name="login" class="input" required/><br/>
      Password:<br/>
      <input type="password" name="pass" class="input" required/><br/>
      <input type="submit" value="Log in" class="input"/><br/>
      <?php
        if(isset($_SESSION['error'])){
          echo "<span style='color: red;'>".$_SESSION['error']."</span>";
          unset($_SESSION['error']);
        }
      ?>
      </form>
    </div>
  </body>
</html>
