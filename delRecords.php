<?php
  session_start();
  require_once('connection.php');
  if(!isset($_SESSION['logged'])){
    header('location: login.php');
    exit();
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <link type="text/css" rel="stylesheet" href="style.css"/>
    <link rel="stylesheet" href="./normalize.css"/>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:600" rel="stylesheet"/>
  </head>
  <body>
    <?php
      try{
        $conn = new PDO("mysql:host=$host;dbname=$db_name", $db_user, $db_pass);
        $paramTable = $conn->prepare("SELECT DISTINCT TABLE_NAME FROM information_schema.COLUMNS WHERE TABLE_NAME='authors' OR TABLE_NAME='years' OR TABLE_NAME='projects'");
        $paramTable->execute();
        $table = $paramTable->fetchAll(PDO::FETCH_COLUMN, 0);
        echo '<div id="mainRecords">';
        for($z = 0; $z < count($table); $z++){
          $paramValues = $conn->prepare("SELECT * FROM $table[$z]");
          $paramValues->execute();
          $values = $paramValues->fetchAll();
          $paramCol = $conn->prepare("SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_SCHEMA='grafika' AND TABLE_NAME=?");
          $paramCol->execute([$table[$z]]);
          $col = $paramCol->fetchAll(PDO::FETCH_COLUMN, 0);
          echo '<div class="tables"><div style="text-align: center">'.$table[$z].'</div><table>';
          echo '<tr>';
          for($i = 0; $i < count($col); $i++){
            echo '<td>'.$col[$i].'</td>';
          }
          echo '<td></td></tr>';
            for($i = 0; $i < count($values); $i++){
              echo '<tr><form method="post" action="delete.php">';
              for($y = 0; $y < (count($values[$i])-count($values[$i])/2); $y++){
                if($table[$z] == 'projects'){
                  if($y == 2){
                    $paramAuthors = $conn->prepare("SELECT CONCAT(name, ' ', surname, ' ', class) FROM authors WHERE authorID=?");
                    $paramAuthors->execute([$values[$i][$y]]);
                    $authors = $paramAuthors->fetchAll(PDO::FETCH_COLUMN, 0);
                    echo '<td><input type="hidden" name="'.$col[$y].'" value="'.$values[$i][$y].'"/>'.$authors[0].'</td>';
                  }else if($y == 3){
                    $paramTypes = $conn->prepare("SELECT name FROM typesOfProjects WHERE typeID=?");
                    $paramTypes->execute([$values[$i][$y]]);
                    $types = $paramTypes->fetchAll(PDO::FETCH_COLUMN, 0);
                    echo '<td><input type="hidden" name="'.$col[$y].'" value="'.$values[$i][$y].'"/>'.$types[0].'</td>';
                  }else if($y == 4){
                    $paramYears = $conn->prepare("SELECT name FROM years WHERE yearID=?");
                    $paramYears->execute([$values[$i][$y]]);
                    $years = $paramYears->fetchAll(PDO::FETCH_COLUMN, 0);
                    echo '<td><input type="hidden" name="'.$col[$y].'" value="'.$values[$i][$y].'"/>'.$years[0].'</td>';
                  }else{
                    echo '<td><input type="hidden" name="'.$col[$y].'" value="'.$values[$i][$y].'"/>'.$values[$i][$y].'</td>';
                  }
                }else{
                  echo '<td><input type="hidden" name="'.$col[$y].'" value="'.$values[$i][$y].'"/>'.$values[$i][$y].'</td>';
                }
              }
              echo '<td><input type="submit" class="textButton" value="Usuń rekord"/></td></form></tr>';
            }
          echo '</table></div>';
        }
        echo '</div>';
      }catch(PDOExeption $e){
        echo "Connection error: ".$e->getMessage();
      }
      $conn = NULL;
    ?>
  </body>
</html>
