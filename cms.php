<?php
  session_start();
  require_once("connection.php");
  if (!isset($_SESSION['logged'])) {
      header("location: login.php");
      exit();
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="./style.css"/>
    <link rel="stylesheet" href="./cms.css"/>
		<link rel="stylesheet" href="./normalize.css"/>
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:600" rel="stylesheet"/>
    <script src="cms.js"></script>
    <?php
      if(isset($_SESSION['error'])){
        echo '<script>
          alert("'.$_SESSION['error'].'");
        </script>';
        unset($_SESSION['error']);
      }else if(isset($_SESSION['success'])){
        echo '<script>
          alert("'.$_SESSION['success'].'");
        </script>';
        unset($_SESSION['success']);
      }
    ?>
  </head>
<body>
    <div id="cms">
      <div id="mainpage">
        <div class="cmsButtons" id="year" onclick="show('addYear', 'year')">
          Dodaj rok
        </div>
        <div class="forms" id="addYear">
          <form action="add.php" method="post">
            Dodaj rok:<br/>
            <input type="text" name="values[]" class="input" pattern="[0-9]{4}\/[0-9]{4}" title="Wzór: 2018/2019" required/><br/>
            <input type="submit" name="years" value="Dodaj"/>
          </form>
        </div>
        <div class="cmsButtons" id="author" onclick="show('addAuthor', 'author')">
          Dodaj autora
        </div>
        <div class="forms" id="addAuthor">
          <form action="add.php" method="post">
            Dodaj imie autora:<br/>
            <input type="text" name="values[]" class="input" required/><br/>
            Dodaj nazwisko autora:<br/>
            <input type="text" name="values[]" class="input" required/><br/>
            Dodaj klasę autora:<br/>
            <input type="text" name="values[]" class="input" pattern="[1-5]{1}[A-Z]{1}" title="Wzór: 4F"/><br/>
            <input type="submit" name="authors" value="Dodaj"/>
          </form>
        </div>
        <div class="cmsButtons" id="project" onclick="show('addProject', 'project')">
          Dodaj projekt
        </div>
        <div class="forms" id="addProject">
          <form action="addProject.php" method="post" enctype="multipart/form-data">
              <?php
                try{
                  $conn = new PDO("mysql:host=$host;dbname=$db_name", $db_user, $db_pass);
                  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                  $paramAuthors = $conn->prepare("SELECT CONCAT(surname, ' ', name, ' ', class) AS author FROM authors ORDER BY surname ASC, name");
                  $paramAuthors->execute();
                  $authors = $paramAuthors->fetchAll(PDO::FETCH_COLUMN, 0);
                  $authorID = array();
                  $paramNames = $conn->prepare("SELECT name, surname FROM authors ORDER BY surname ASC, name");
                  $paramNames->execute();
                  $names = $paramNames->fetchAll();
                  for($i = 0; $i < count($authors); $i++){
                    $paramID = $conn->prepare("SELECT authorID FROM authors WHERE name=? AND surname=?");
                    $paramID->execute([$names[$i][0], $names[$i][1]]);
                    $authorID = array_merge($authorID, $paramID->fetchAll(PDO::FETCH_COLUMN, 0));
                  }
                  echo '<div id="projectAuthors">';
                  echo '<select name="author[]" class="input">';
                  for($i = 0; $i < count($authors); $i++){
                    echo '<option value="'.$authorID[$i].'">'.$authors[$i].'</option>';
                  }
                  echo '</select><br/></div><input type="button" value="Dodaj kolejnego autora" onclick="addAuthor()"/>';
                  echo '<script>
                    function addAuthor(){
                      var author = document.getElementById(\'projectAuthors\');
                      var text = author.innerHTML;
                      var length = text.search("<br>");
                      text += text.slice(0, (length+4));
                      author.innerHTML = text;
                    }
                  </script>';
                  $paramTypes = $conn->prepare("SELECT name FROM typesOfProjects");
                  $paramTypes->execute();
                  $types = $paramTypes->fetchAll(PDO::FETCH_COLUMN, 0);
                  $paramTypesID = $conn->prepare("SELECT typeID FROM typesOfProjects");
                  $paramTypesID->execute();
                  $paramID = $paramTypesID->fetchAll(PDO::FETCH_COLUMN, 0);
                  echo '<select name="type" class="input">';
                  for($i = 0; $i < count($types); $i++){
                    echo '<option value="'.$paramID[$i].'">'.$types[$i].'</option>';
                  }
                  echo '</select><br/>';
                  $paramYears = $conn->prepare("SELECT name FROM years");
                  $paramYears->execute();
                  $years = $paramYears->fetchAll(PDO::FETCH_COLUMN, 0);
                  $paramYearsID = $conn->prepare("SELECT yearID FROM years");
                  $paramYearsID->execute();
                  $yearsID = $paramYearsID->fetchAll(PDO::FETCH_COLUMN, 0);
                  echo '<select name="year" class="input">';
                  for($i = 0; $i < count($years); $i++){
                    echo '<option value="'.$yearsID[$i].'">'.$years[$i].'</option>';
                  }
                  echo '</select><br/>';
                }catch(PDOExeption $e){
                  echo "Connection failed: ".$e->getMessage();
                }
                $conn = NULL;
              ?>
            Dodaj dodatkowe informacje<br/>
            <input type="text" name="extra" class="input"/><br/>
            Dodaj pracę<br/>
            <input type="file" name="projectFile" class="input" required/><br/>
            Dodaj miniaturkę<br/>
            <input type="file" name="miniatureFile" class="input" /><br/>
            <input type="submit" value="Dodaj"/>
          </form>
        </div>
        <div class="cmsButtons" id="pass" onclick="show('changePass', 'pass')">
          Zmień hasło
        </div>
        <div class="forms" id="changePass">
          <form action="passChange.php" method="post">
            Podaj nowe hasło<br/>
            <input type="password" name="pass" required/><br/>
            Potwierdź nowe hasło<br/>
            <input type="password" name="passVer" required/><br/>
            <input type="submit" value="Zmień hasło"/>
          </form>
        </div>
        <a href="delRecords.php"><div class="cmsButtons">Usuń rekordy</div></a>
        <a href="logout.php"><div class="cmsButtons">Wyloguj się</div></a>
      </div>
    </div>
  </body>
</html>
