<?php
  session_start();
  require_once('connection.php');
  if(!isset($_SESSION['logged'])){
    header('location: login.php');
    exit();
  }
  if(!isset($_POST['author']) || !isset($_POST['type']) || !isset($_POST['year']) || !isset($_FILES['projectFile']) || !isset($_FILES['miniatureFile'])){
    header('location: cms.php');
    exit();
  }
  try{
    $conn = new PDO("mysql:host=$host;dbname=$db_name", $db_user, $db_pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $name = $_FILES['projectFile']['name'];
    $pathParts = pathinfo($name);
    $trueFile = false;
    $miniTrue = false;
    $miniName = $_FILES['miniatureFile']['name'];
    $miniParts = pathinfo($miniName);
    if($_POST['type'] == 4){
      if($pathParts['extension'] == 'zip') $trueFile = true;
    }else if($_POST['type'] == 1){
      if($pathParts['extension'] == 'mp4') $trueFile = true;
    }else if($_POST['type'] == 2 || $_POST['type'] == 3){
      if($pathParts['extension'] == 'png') $trueFile = true;
      else if($pathParts['extension'] == 'jpg') $trueFile = true;
      else if($pathParts['extension'] == 'jpeg') $trueFile = true;
    }
    if($_POST['type'] == 1){
      $miniTrue = true;
    }else if($miniParts['filename'] == 'mini'){
      if($miniParts['extension'] == 'png') $miniTrue = true;
      else if($miniParts['extension'] == 'jpg') $miniTrue = true;
      else if($miniParts['extension'] == 'jpeg') $miniTrue = true;
    }
    if($trueFile && $miniTrue){
      $paramYear = $conn->prepare("SELECT name FROM years WHERE yearID=?");
      $paramYear->execute([$_POST['year']]);
      $year = $paramYear->fetchAll(PDO::FETCH_COLUMN, 0);
      $year[0] = str_replace('/', '-', $year[0]);
      $paramType = $conn->prepare("SELECT name FROM typesOfProjects WHERE typeID=?");
      $paramType->execute([$_POST['type']]);
      $type = $paramType->fetchAll(PDO::FETCH_COLUMN, 0);
      $type[0] = str_replace(' ', '_', $type[0]);
      $uploadPath = './projects/'.$year[0].'/'.$type[0].'/';
      $dirs = glob($uploadPath.'*', GLOB_ONLYDIR);
      $unique = false;
      if(!$unique){
        $namePath = substr(base64_encode(mt_rand()), 0, 15);
        $unique = true;
      }
      for($i = 0; $i < count($dirs); $i++){
        if($dirs[$i] == $namePath){
          $unique = false;
        }
        if(!$unique){
          $namePath = substr(base64_encode(mt_rand()), 0, 15);
          $unique = true;
        }
      }
      $authors = $_POST['author'];
      /*$paramCheck = $conn->prepare("SELECT projectID FROM projects WHERE authorID=? AND typeID=? AND yearID=?");
      $check = array();
      $paramCheck->execute([$authors[0], $_POST['type'], $_POST['year']]);
      $check = $paramCheck->fetchAll(PDO::FETCH_COLUMN, 0);*/
      if(true){
        if(mkdir($uploadPath.$namePath)){
          if(!$_POST['type'] != 4){
            $extPart = pathinfo($name);
            $ext = $extPart['extension'];
            $name = $namePath.'.'.$ext;
          }
          if(move_uploaded_file($_FILES['projectFile']['tmp_name'], $uploadPath.$namePath.'/'.$name)){
            if($_POST['type'] != 1){
              move_uploaded_file($_FILES['miniatureFile']['tmp_name'], $uploadPath.$namePath.'/'.$_FILES['miniatureFile']['name']);
            }
            if($_POST['type'] == 4){
              $zip = new ZipArchive;
              if($zip->open($uploadPath.$namePath.'/'.$name)){
                $zip->extractTo($uploadPath.$namePath);
                $zip->close();
                unlink($uploadPath.$namePath.'/'.$name);
                $url = $uploadPath.$namePath.'/index.html';
              }else{
                $_SESSION['error'] = "Nie udało się otworzyć pliku zip.";
                rmdir($uploadPath.$namePath);
                header('location: cms.php');
                $conn = NULL;
                exit();
              }
            }else{
              $url = $uploadPath.$namePath.'/'.$name;
            }
            for($i = 0; $i < count($authors); $i++){
              $paramUpload = $conn->prepare("INSERT INTO projects VALUES('0', ?, ?, ?, ?, ?)");
              $paramUpload->execute([$url, $authors[$i], $_POST['type'], $_POST['year'], $_POST['extra']]);
            }
            $_SESSION['success'] = "Projekt został dodany.";
            header('location: cms.php');
          }else{
            $_SESSION['error'] = "Nie udało się przesłać pliku lub przesłać miniaturki.";
            header('location: cms.php');
          }
        }
      }else{
        $_SESSION['error'] = "Istnieje taki już projekt.";
        header('location: cms.php');
      }
    }else{
      if(!$miniTrue){
        $_SESSION['error'] = "Można tylko przesłać plik png w miniaturce i nazwa musi być mini.png.";
      }else if($_POST['type'] == 4){
        $_SESSION['error'] = "Można tylko przesłać plik zip.";
      }else if($_POST['type'] == 1){
        $_SESSION['error'] = "Można tylko przesłać plik mp4.";
      }else if($_POST['type'] == 2 || $_POST['type'] == 3){
        $_SESSION['error'] = "Można tylko przesłać plik jpg, jpeg lub png.";
      }
      header('location: cms.php');
    }
  }catch(PDOExeption $e){
    echo "Connection error: ".$e->getMessage();
  }
  $conn = NULL;
?>
