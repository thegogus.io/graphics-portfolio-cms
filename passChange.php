<?php
  session_start();
  require_once('connection.php');
  if(!isset($_SESSION['logged'])){
    header('location: login.php');
    exit();
  }
  if(!isset($_POST['pass']) || !isset($_POST['passVer'])){
    header('location: cms.php');
    exit();
  }
  if($_POST['pass'] != $_POST['passVer']){
    $_SESSION['error'] = "Hasła się nie zgadzają.";
    header('location: cms.php');
    exit();
  }
  try{
    $conn = new PDO("mysql:host=$host;dbname=$db_name", $db_user, $db_pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = 'UPDATE accounts SET pass=? WHERE id=\'1\'';
    $paramPass = $conn->prepare($sql);
    $hash = password_hash($_POST['pass'], PASSWORD_DEFAULT);
    $paramPass->execute([$hash]);
    header('location: logout.php');
  }catch(PDOExeption $e){
    echo 'Connection failed: '.$e->getMessage();
  }
  $conn = NULL;
?>
