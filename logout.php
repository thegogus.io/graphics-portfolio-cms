<?php
  session_start();
  if(!isset($_SESSION['logged'])){
    header("location: login.php");
    exit();
  }
  session_unset();
  header("location: login.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
  </head>
  <body>
  </body>
</html>
