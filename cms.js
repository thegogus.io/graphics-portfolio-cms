function show(formElement, element){
    var ids = ['addAuthor', 'author', 'addProject', 'project', 'addYear', 'year', 'changePass', 'pass'];
    for(i = 0; i < ids.length; i++){
      if(ids[i] == formElement){
          ids.splice(i, 2);
      }
    }
    for(i = 0; i < ids.length; i+=2){
      document.getElementById(ids[i]).style.display = 'none';
      document.getElementById(ids[i+1]).style.display = 'flex';
    }
    var hide = document.getElementById(element);
    var show = document.getElementById(formElement);
    hide.style.display = 'none';
    show.style.display = 'initial';
}
