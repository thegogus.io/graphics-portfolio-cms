<?php
  session_start();
  require_once('connection.php');
  if(!isset($_SESSION['logged'])){
    header('location: login.php');
    exit();
  }
  if(!isset($_POST['authorID']) && !isset($_POST['url']) && !isset($_POST['yearID'])){
    header('location: cms.php');
    exit();
  }
  try{
    if(isset($_POST['url'])){
      $conn = new PDO("mysql:host=$host;dbname=$db_name", $db_user, $db_pass);
      $projFolder = dirname($_POST['url'], 1);
      $it = new RecursiveDirectoryIterator($projFolder, RecursiveDirectoryIterator::SKIP_DOTS);
      $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
      foreach($files as $file){
        if($file->isDir()){
          rmdir($file->getRealPath());
        }else{
          unlink($file->getRealPath());
        }
      }
      rmdir($projFolder);
      $paramDelProj = $conn->prepare("DELETE FROM projects WHERE url=?");
      $paramDelProj->execute([$_POST['url']]);
    }else if(isset($_POST['yearID'])){
      $conn = new PDO("mysql:host=$host;dbname=$db_name", $db_user, $db_pass);
      $paramYear = $conn->prepare("SELECT name FROM years WHERE yearID=?");
      $paramYear->execute([$_POST['yearID']]);
      $year = $paramYear->fetchAll(PDO::FETCH_COLUMN, 0);
      $yearFolder = './projects/'.str_replace('/', '-', $year[0]).'/';
      $it = new RecursiveDirectoryIterator($yearFolder, RecursiveDirectoryIterator::SKIP_DOTS);
      $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
      foreach($files as $file){
        if($file->isDir()){
          rmdir($file->getRealPath());
        }else{
          unlink($file->getRealPath());
        }
      }
      rmdir($yearFolder);
      $paramDelYear = $conn->prepare("DELETE FROM years WHERE yearID=?");
      $paramDelYear->execute([$_POST['yearID']]);
    }else if(isset($_POST['authorID'])){
      $paramDelAuthor = $conn->prepare("DELETE FROM authors WHERE authorID=?");
      $paramDelAuthor->execute([$_POST['authorID']]);
    }
    $_SESSION['success'] = "Usunięto rekordy.";
    header('location: cms.php');
  }catch(PDOExeption $e){
    echo 'Connection failed: '.$e->getMessage();
  }
  $conn = NULL;
?>
