<?php
  session_start();
  require_once 'connection.php';
  if(!isset($_SESSION['logged'])){
    header('location: login.php');
    exit();
  }
  if(!isset($_POST['values'])){
    header('location: login.php');
    exit();
  }
  try{
    $conn = new PDO("mysql:host=$host;dbname=$db_name", $db_user, $db_pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $values = $_POST['values'];
    if(isset($_POST['years'])){
      $table = "years";
    }else if(isset($_POST['authors'])){
      $table = "authors";
    }
    $sqlCol = 'SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME=? AND TABLE_SCHEMA=\'grafika\'';
    $paramCol = $conn->prepare($sqlCol);
    $paramCol->execute([$table]);
    $colNames = $paramCol->fetchAll(PDO::FETCH_COLUMN, 0);
    array_splice($colNames, 0, 1);
    $sqlChk = "";
    for($i = 0; $i < count($colNames); $i++){
      if($i == count($colNames)-1){
        $sqlChk .= $colNames[$i].'=?';
      }else{
        $sqlChk .= $colNames[$i].'=? AND ';
      }
    }
    $sqlChkQue = 'SELECT * FROM '.$table.' WHERE '.$sqlChk;
    $paramChk = $conn->prepare($sqlChkQue);
    $paramChk->execute($values);
    $resultChk = $paramChk->fetchAll(PDO::FETCH_COLUMN, 0);
    for($i = 0; $i < count($values); $i++){
      $input .= ', ?';
    }
    if(count($resultChk) > 0){
      $_SESSION['error'] = "Już jest taki rekord w bazie.";
      header("location: cms.php");
      $conn = NULL;
      exit();
    }else{
      if(isset($_POST['years'])){
        $folder = str_replace('/', '-', $values[0]);
        $path = './projects/'.$folder;
        $subPath = $path;
        mkdir($path);
        $sqlTypes = 'SELECT name FROM typesOfProjects';
        $paramTypes = $conn->prepare($sqlTypes);
        $paramTypes->execute();
        $types = $paramTypes->fetchAll(PDO::FETCH_COLUMN, 0);
        for($i = 0; $i < count($types); $i++){
          $subPath = $path;
          $subFolder = str_replace(' ', '_', $types[$i]);
          $subPath .= '/'.$subFolder;
          mkdir($subPath);
        }
      }
      $sql = 'INSERT INTO '.$table.' VALUES(\'0\''.$input.')';
      $paramAdd = $conn->prepare($sql);
      $paramAdd->execute($values);
      $_SESSION['success'] = "Rekordy zostały dodane.";
      header("location: cms.php");
    }
  }catch(PDOExeption $e){
    echo "Connection failed: ".$e.getMessage();
  }
  $conn = NULL;
?>
