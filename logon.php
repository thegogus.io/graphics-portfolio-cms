<?php
  session_start();
  require_once("connection.php");
  if(isset($_SESSION['logged'])){
    header("location: cms.php");
    exit();
  }
  if(!isset($_POST['login']) || !isset($_POST['pass'])){
    header("location: login.php");
    exit();
  }
  try{
      $conn = new PDO("mysql:host=$host;dbname=$db_name", $db_user, $db_pass);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $paramHash = $conn->prepare("SELECT pass FROM accounts WHERE name=?");
      $paramHash->execute([$_POST['login']]);
      $hash = $paramHash->fetch();
      if(password_verify($_POST['pass'], $hash[0])){
        $_SESSION['logged'] = 1;
        header("location: cms.php");
      }else{
        $_SESSION['error'] = "Zły login lub hasło.";
        header("location: login.php");
      }
  }catch(PDOExeption $e){
    echo "Connection failed: ".$e->getMessage();
  }
  $conn = NULL;
?>
